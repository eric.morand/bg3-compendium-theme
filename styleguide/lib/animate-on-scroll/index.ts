import {applyAnimateOnScroll} from "../../../src/lib/components/animate-on-scroll";

document.addEventListener("DOMContentLoaded", () => {
   const testCases = document.getElementsByClassName('test-case');

   for (const testCase of testCases) {
       applyAnimateOnScroll(testCase as HTMLElement);
   }
});