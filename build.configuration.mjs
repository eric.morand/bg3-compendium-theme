import {createRollupPlugin} from "@stromboli/plugin-rollup";
import createTypeScriptPlugin from "@rollup/plugin-typescript";
import createNodeResolvePlugin from "@rollup/plugin-node-resolve";
import {createSassPlugin} from "@stromboli/plugin-sass";
import createCommonJSPlugin from "@rollup/plugin-commonjs";
import {readFileSync} from "fs";
import {join, dirname} from "path";
import {createANDMiddleware} from "@arabesque/logic-middlewares";
import {createCssSourceMapRebasePlugin} from "@stromboli/plugin-css-source-map-rebase";

/**
 * @type {import("@stromboli/core").Plugin}
 */
const virtualRollupPlugin = (context, next) => {
  const rollupPlugin = createRollupPlugin({
    plugins: [
      createTypeScriptPlugin(),
      createNodeResolvePlugin(),
      createCommonJSPlugin()
    ]
  });

  const {component} = context;
  const {name, path} = component;

  const components = [
    `entry/index.ts`
  ];

  console.log(name, path);

  context.component = {
    name,
    path,
    data: components.map((component) => {
      return `import "${component}";`;
    }).join('')
  };

  return rollupPlugin(context, next);
}

/**
 * @type {import("@stromboli/core").Plugin}
 */
const virtualCopyTemplatePlugin = (context, next) => {
  const {component} = context;
  const {name, path} = component;

  const components = [
    'entry',
    'html'
  ];

  for (const component of components) {
    context.response.artifacts.push({
      name: `${component}.html.twig`,
      data: readFileSync(join(dirname(path), `${component}/index.html.twig`))
    });
  }

  return next(context);
}

const routes = [
  {
    component: {
      name: 'style',
      path: 'index.scss'
    },
    plugin: createANDMiddleware(
      createSassPlugin(),
      createCssSourceMapRebasePlugin()
    ),
    output: (artifact) => {
      if (artifact.name === 'style') {
        return 'assets/index.css';
      }
      else {
        return `assets/${artifact.name}`;
      }
    }
  },
  {
    component: {
      name: 'behaviour',
      path: 'index.ts'
    },
    plugin: virtualRollupPlugin,
    output: 'assets/index.mjs'
  },
  {
    component: {
      name: 'content',
      path: 'index.html.twig'
    },
    plugin: virtualCopyTemplatePlugin,
    output: (artifact) => {
      return `templates/${artifact.name}`;
    }
  }
]

/**
 * @type {import("@stromboli/cli").Configuration}
 */
const configuration = {
  outputDirectory: 'dist',
  routes
};

export default configuration;