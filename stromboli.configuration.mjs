import {createTwingPlugin} from "@stromboli/plugin-twing";
import {TwingLoaderChain, TwingLoaderRelativeFilesystem} from "twing";
import {createRollupPlugin} from "@stromboli/plugin-rollup";
import createTypeScriptPlugin from "@rollup/plugin-typescript";
import createNodeResolvePlugin from "@rollup/plugin-node-resolve";
import {createSassPlugin} from "@stromboli/plugin-sass";
import {createANDMiddleware} from "@arabesque/logic-middlewares";
import createCommonJSPlugin from "@rollup/plugin-commonjs";
import {createCssSourceMapRebasePlugin} from "@stromboli/plugin-css-source-map-rebase";

/** @type {() => import("@stromboli/core").Plugin} */
const createBannerPlugin = () => {
    return (context, next) => {
        const artifacts = [];

        for (const {name, data, sourceMap} of context.response.artifacts) {
            artifacts.push({
                name,
                data: Buffer.concat([
                    Buffer.from('foo'),
                    data
                ]),
                sourceMap
            });
        }

        context.response.artifacts = [...context.response.artifacts, ...artifacts];

        return next(context);
    }
}

/**
 * @type {import("@stromboli/cli").Configuration}
 */
const configuration = {
    outputDirectory: 'www',
    routes: [
        {
            component: {
                name: 'content',
                path: 'index.html.twig'
            },
            plugin: createANDMiddleware(
                createTwingPlugin(new TwingLoaderChain([
                    new TwingLoaderRelativeFilesystem()
                ]))
            ),
            output: 'index.html'
        },
        {
            component: {
                name: 'behaviour',
                path: 'index.ts'
            },
            plugin: createRollupPlugin({
                plugins: [
                    createTypeScriptPlugin(),
                    createNodeResolvePlugin(),
                    createCommonJSPlugin()
                ]
            }),
            output: 'index.mjs'
        },
        {
            component: {
                name: 'style',
                path: 'index.scss'
            },
            plugin: createANDMiddleware(
              createSassPlugin(),
              createCssSourceMapRebasePlugin((s, r, done) => {
                console.log(s, r);

                done();
              })
            ),
            output: (artifact) => {
                if (artifact.name === 'style') {
                  return 'index.css';
                }
                else {
                  return artifact.name;
                }
            }
        }
    ]
};

export default configuration;