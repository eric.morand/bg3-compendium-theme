import {applyAnimateOnScroll} from "../../lib/components/animate-on-scroll";

document.addEventListener("DOMContentLoaded", () => {
    const entries = document.getElementsByClassName('entry');

    for (const entry of entries) {
        const spriteSheets = entry.getElementsByClassName('sprite-sheet');

        for (const spriteSheet of spriteSheets) {
            applyAnimateOnScroll(spriteSheet as HTMLElement);
        }
    }
});