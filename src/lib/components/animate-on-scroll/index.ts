export type AnimateOnScroll = {};

export const applyAnimateOnScroll = (
    element: HTMLElement
): AnimateOnScroll => {
    const frames = Array.from(element.getElementsByTagName('img')) as Array<HTMLElement>;

    if (frames.length > 0) {
        const numberOfFrames = frames.length;
        const threshold = Number.parseInt(element.dataset["threshold"] || '100');

        let currentFrame: HTMLElement | null = null;

        const setCurrentFrame = (frame: HTMLElement) => {
            if (currentFrame !== null) {
                currentFrame.classList.remove("current");
            }

            element.style.height = `${numberOfFrames * threshold + frame.offsetHeight}px`;

            frame.classList.add("current");

            currentFrame = frame;
        };

        setCurrentFrame(frames[0]);

        window.addEventListener("scroll", () => {
            const relativeScrollPosition = Math.max(window.scrollY - element.offsetTop, 0);
            const index = Math.min(Math.floor(relativeScrollPosition / threshold), frames.length - 1);
            const frame = frames[index];

            if (frame && (frame !== currentFrame)) {
                setCurrentFrame(frame);
            }
        });
    }

    return {};
};